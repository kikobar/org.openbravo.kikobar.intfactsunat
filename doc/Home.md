**We changed the project approach to a query extraction. The PostgreSQL query and the reference data are included in the source code, but you will need to write your extraction code in your preferred programming language according to your own design and workflow.**

The extraction script will need to read the query from the database and write the JSON files in a location where the Facturador SUNAT can ingest them from.

We have accomplished the above in Python with Psycopg2, but it can be done in any language capable of interacting with PostgreSQL.

----------------

**References**
| Reference | Link |
| ------ | ------ |
| SUNAT Comprobante de Pago Electronico microsite | [[https://cpe.sunat.gob.pe]] |
| SUNAT | [[https://www.sunat.gob.pe/]] |
| Facturador download | [[https://cpe.sunat.gob.pe/sites/default/files/inline-files/Instaladores%20del%20sistema%20facturador%20SUNAT_ultima_version_30_09_2021.pdf]] |
| Facturador Documentation | [[https://www.gob.pe/institucion/sunat/informes-publicaciones/1893360-instructivo-para-la-instalacion-del-facturador-sunat]] [AnexosIyII_Formato1.3.xlsx](uploads/3180988db1995802ee4713aedb778c9f/AnexosIyII_Formato1.3.xlsx) [anexo1-193-2020.pdf](uploads/7f12665fa2aa57640692cdb9db1c9c88/anexo1-193-2020.pdf)[anexo2-193-2020.pdf](uploads/90058b1cfecf6e95f2f8dadeb2d05eec/anexo2-193-2020.pdf) [anexo3-193-2020.pdf](uploads/e6ef54bcc2ed3f2dc94f9bab4df88e91/anexo3-193-2020.pdf)[anexo4-193-2020.pdf](uploads/0dd80853ad57009c38d9e19e64a4a286/anexo4-193-2020.pdf)[anexo5-193-2020.pdf](uploads/a0bb2044ed878d752d49338ac9d17833/anexo5-193-2020.pdf)[anexo6-193-2020.pdf](uploads/e87fc3aba4db2e0642f3cf3e3fb66feb/anexo6-193-2020.pdf)[anexoIII.pdf](uploads/be896f0a6606c16da03f775cb753afa5/anexoIII.pdf)[AjustesValidacionesCPEv20201207_2.xlsx](uploads/0b385844d734442c66331fdd462bb07b/AjustesValidacionesCPEv20201207_2.xlsx)[AnexosIyII_Formato1.4.xlsx](uploads/9658a36064941c27dfb3a5ff5d19be6b/AnexosIyII_Formato1.4.xlsx)|
|Universal Business Language | [[http://www.datypic.com/sc/ubl21/ss.html]] |
| SUNAT API for queries on Facturas Electronicas | https://cpe.sunat.gob.pe/sites/default/files/inline-files/Manual-de-Consulta-Integrada-de-Comprobante-de-Pago-por-ServicioWEB_v2.pdf |