﻿-- This query extract invoices from the Openbravo Postgres database to generate
-- a JSON document for submitting through the FACTURADOR SUNAT application.
-- FACTURADOR SUNAT is a free application for generating Facturas Electronicas SUNAT.

create temp table if not exists mensaje (
	codigo varchar(4),
	mens varchar(100)
	);
truncate mensaje;
insert into mensaje (codigo, mens)
values ('1000','motary value in text'),
	('2006','Operación sujeta a detracción'),
	('NNNN','Any other required legend');

with
ventas as (

select division.name as organisation,parent.name as parentorg,ad_orginfo.taxid as ruc,
ad_client.name as client,documentno,c_invoice.description,dateinvoiced,totallines,grandtotal,
qtyinvoiced,priceactual,
linenetamt,c_invoiceline.taxbaseamt,m_product.value as productcode,m_product.name as productname,
c_invoiceline.description as productdesc, iso_code,
c_invoiceline.line,c_invoice.c_invoice_id,c_invoiceline.c_invoiceline_id,
c_bpartner.name as bpname,c_bpartner.taxid,c_uom.name as uom,c_doctype.name as doctype,
c_paymentterm.name as paymentterm,c_paymentterm.netdays,
c_invoice.isactive,c_invoice.issotrx,
c_invoicetax.taxbaseamt as invoicetaxbaseamt,c_invoicetax.taxamt as invoicetaxtaxamt,
taxinvoice.name,
c_invoicelinetax.taxbaseamt as linetaxbaseamt,c_invoicelinetax.taxamt as linetaxtaxamt,
taxline.name,
fin_payment_schedule.amount as cuota, fin_payment_schedule.duedate, fin_payment_schedule.fin_payment_schedule_id,
concat(c_location.address1,' ',c_location.address2,' ',c_location.city,' ',c_location.postal,' ',c_country.name) as direccioncliente,
codigo, mens


from
c_invoice
left join c_invoiceline on c_invoiceline.c_invoice_id = c_invoice.c_invoice_id
left join m_product on m_product.m_product_id = c_invoiceline.m_product_id
left join ad_client on ad_client.ad_client_id = c_invoice.ad_client_id
left join c_currency on c_currency.c_currency_id = c_invoice.c_currency_id
left join c_bpartner on c_bpartner.c_bpartner_id = c_invoice.c_bpartner_id
left join c_uom on c_uom.c_uom_id = c_invoiceline.c_uom_id
left join c_doctype on c_doctype.c_doctype_id = c_invoice.c_doctype_id
left join c_paymentterm on c_paymentterm.c_paymentterm_id = c_invoice.c_paymentterm_id
left join c_invoicetax on c_invoicetax.c_invoice_id = c_invoice.c_invoice_id
left join c_tax as taxinvoice on taxinvoice.c_tax_id = c_invoicetax.c_tax_id
left join c_invoicelinetax on c_invoicelinetax.c_invoiceline_id = c_invoiceline.c_invoiceline_id
left join c_tax as taxline on taxline.c_tax_id = c_invoicelinetax.c_tax_id
left join ad_org as division on division.ad_org_id = c_invoice.ad_org_id
left join ad_org as parent on parent.ad_org_id = division.ad_legalentity_org_id
left join ad_orginfo on ad_orginfo.ad_org_id = parent.ad_org_id
left join fin_payment_schedule on fin_payment_schedule.c_invoice_id = c_invoice.c_invoice_id
left join c_bpartner_location on c_bpartner_location.c_bpartner_location_id = c_invoice.c_bpartner_location_id
left join c_location on c_location.c_location_id = c_bpartner_location.c_location_id
left join c_country on c_country.c_country_id = c_location.c_country_id
full outer join mensaje on true


where
c_invoice.documentno = 'E001-27' and
--c_invoice.documentno = '1000052' and
c_invoice.issotrx = 'Y' and
c_invoice.isactive = 'Y' and
c_invoice.docstatus = 'CO' and
ad_client.name ='BLB'
--ad_client.name ='Locate & Grow'
--ad_client.name ='Uchu Wasi'
)

-- Above this line is the full query select all the required invoice data
-- Below this line a JSON sturcture is created to be feed into the FACTURADOR SUNAT APP.


--select *


--/*
select

	(jsonb_build_object(
		'cabecera', jsonb_agg(DISTINCT
			jsonb_build_object(
				'ivoiceid',c_invoice_id,
				'tipOperacion','1001',
				'fecEmision',dateinvoiced::date,
				'horEmision',dateinvoiced::time,
				'codLocalEmisor','0001',
				'tipDocUsuario',case
					when length(taxid)=11 then '6'
					when length(taxid)=8 then '1'
					else '0' end,
				'numDocUsuario',taxid,
				'rznSocialUsuario',bpname,
				'tipMoneda',iso_code,
				'codMotivo',case
					when substring(doctype,1,4)='Nota' then '01'
					else '-' end,
				'desMotivo',case
					when substring(doctype,1,4)='Nota' then 'Anulacion de operacion'
					else '-' end,
				'tipDocAfectado',case
					when substring(doctype,1,4)='Nota' then '01'
					else '-' end,
				'numDocAfectado',case
					when substring(doctype,1,4)='Nota' then description
					else '-' end,
				'sumTotTributos',(grandtotal-totallines)::varchar,
				'sumTotValVenta',totallines::varchar,
				'sumPrecioVenta',grandtotal::varchar,
				'sumDescTotal','0.00',
				'sumOtrosCargos','0.00',
				'sumTotalAnticipos','0.00',
				'sumImpVenta',grandtotal::varchar,
				'ublVersionId','2.1',
				'customizationId','2.0',
				'adicionalCabecera', jsonb_build_object(
					'invoiceid',c_invoice_id,
					'codPaisCliente','PE',
					'codUbigeoCliente','150140',
					'desDireccionCliente',direccioncliente,
					'codPaisEntrega','PE',
					'codUbigeoEntrega','150140',
					'desDireccionEntrega',direccioncliente,
					'ctaBancoNacionDetraccion','11111111111',
					'codBienDetraccion','037',
					'porDetraccion','10.0',
					'mtoDetraccion',round(grandtotal*0.1,2)::varchar,
					'codMedioPago','003'
					)
				)
			)->0
		)
		||
	jsonb_build_object(
		'detalle', jsonb_agg(DISTINCT
			jsonb_build_object(
				'invoiceline',c_invoiceline_id,
				'codUnidadMedida','BX',
				'ctdUnidadItem',qtyinvoiced::varchar,
				'codProducto',coalesce(productcode,'-'),
				'codProductoSUNAT','50161813',
				'desItem',coalesce(productname,productdesc,'-'),
				'mtoValorUnitario',priceactual::varchar,
				'sumTotTributosItem',linetaxtaxamt::varchar,

				'codTriIGV','1000',
				'mtoIgvItem',linetaxtaxamt::varchar,
				'mtoBaseIgvItem',linetaxbaseamt::varchar,
				'nomTributoIgvItem','IGV',
				'codTipTributoIgvItem','VAT',
				'tipAfeIGV','10',
				'porIgvItem','18.00',

				'codTriISC','2000',
				'mtoIscItem','0.00',
				'mtoBaseIscItem','0.00',
				'nomTributoIscItem','ISC',
				'codTipTributoIscItem','EXC',
				'tipSisISC','02',
				'porIscItem','0.00',

				'codTriOtro','9999',
				'mtoTriOtroItem','0.00',
				'mtoBaseTriOtroItem','0.00',
				'nomTributoOtroItem','OTROS',
				'codTipTributoOtroItem','OTH',
				'porTriOtroItem','0.00',

--				'codTriIcbper','7152',
				'codTriIcbper','-',
				'mtoTriIcbperItem','0.00',
				'ctdBolsasTriIcbperItem','0',
				'nomTributoIcbperItem','ICBPER',
				'codTipTributoIcbperItem','OTH',
				'mtoTriIcbperUnidad','0.30',

				'mtoPrecioVentaUnitario',round((linenetamt+linetaxtaxamt)/qtyinvoiced,2)::varchar,
				'mtoValorVentaItem',linenetamt::varchar,
				'mtoValorReferencialUnitario','0.00'
				)
			)
		)
		||
	jsonb_build_object(
		'adicionalDetalle','[]'::jsonb
		)
		||
	jsonb_build_object(
		'tributos', jsonb_agg(DISTINCT
			jsonb_build_object(
				'ideTributo','1000',
				'nomTributo','IGV',
				'codTipTributo','VAT',
				'mtoBaseImponible',invoicetaxbaseamt::varchar,
				'mtoTributo',invoicetaxtaxamt::varchar
				)
			)
		)
		||
	jsonb_build_object(
		'leyendas', jsonb_agg(DISTINCT
			jsonb_build_object(
				'codLeyenda', codigo,
				'desLeyenda', mens
				)
			)
		)
		||
	jsonb_build_object(
		'relacionados','[]'::jsonb
		)
		||
	jsonb_build_object(
		'variablesGlobales','[]'::jsonb
		)
		||
	case
		when substring(doctype,1,4)='Bole' then jsonb_build_object('datoPagoX','[]'::jsonb)
		else
		jsonb_build_object(
			'datoPago', jsonb_agg(DISTINCT
				jsonb_build_object(
					'formaPago','Credito',
					'mtoNetoPendientePago',grandtotal::varchar,
					'tipMonedaMtoNetoPendientePago',iso_code
					)
				)->0
			)
		end
		||
	case
		when substring(doctype,1,4)='Bole' then jsonb_build_object('detallePagoX','[]'::jsonb)
		else
		jsonb_build_object(
			'detallePago', jsonb_agg(DISTINCT
				jsonb_build_object(
					'cuotaid',fin_payment_schedule_id,
					'mtoCuotaPago',abs(cuota)::varchar,
					'fecCuotaPago',duedate::date,
					'tipMonedaCuotaPago',iso_code
					)
				)
			)
		end
		||
	jsonb_build_object(
		'retencion', jsonb_agg(DISTINCT
			jsonb_build_object(
				'impOperacion',grandtotal::varchar,
				'porRetencion','0.00',
				'impRetencion','0.00'
				)
			)->0
		)
	)::varchar,doctype,ruc

--*/

from ventas
group by doctype,ruc;
